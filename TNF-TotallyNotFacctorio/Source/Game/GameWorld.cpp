#include "stdafx.h"
#include "GameWorld.h"
#include <tga2d/sprite/sprite.h>
#include <tga2d/error/error_manager.h>
#include <tga2d/sprite/sprite_batch.h>

CGameWorld::CGameWorld()
{
	//construct
}


CGameWorld::~CGameWorld() 
{
	//unload
}

void CGameWorld::Init()  
{
	//init/load
}


void CGameWorld::Update(float /*aTimeDelta*/)
{ 	
	if (GetAsyncKeyState(VK_ESCAPE))
	{
		Tga2D::CEngine::Shutdown();
	}
}
