#pragma once
#include <map>
#include <array>
#include <tga2d/sprite/sprite_batch.h>

namespace Tga2D
{
	class CSprite;
}

class CGameWorld
{
public:
	CGameWorld(); 
	~CGameWorld();

	void Init();
	void Update(float aTimeDelta); 

private:

};